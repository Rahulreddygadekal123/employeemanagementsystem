package com.ems.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.*;
import org.springframework.transaction.annotation.Transactional;

import com.ems.dao.AdminDAO;
import com.ems.model.Employee;

@Service
public class AdminServiceImpl implements AdminService {

	@Autowired
	private AdminDAO adminDAO;

	@Transactional
	@Override
	public List<Employee> allEmployees() {
		
		return adminDAO.allEmployees();
	}

	@Transactional
	@Override
	public void updateEmployee(Employee employee) {
		// TODO Auto-generated method stub
		adminDAO.updateEmployee(employee);
		
	}

}
