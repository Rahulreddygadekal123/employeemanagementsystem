package com.ems.service;

import java.util.List;

import com.ems.model.Employee;

public interface AdminService {

	 List<Employee> allEmployees();
	 void updateEmployee(Employee employee);
	
}
