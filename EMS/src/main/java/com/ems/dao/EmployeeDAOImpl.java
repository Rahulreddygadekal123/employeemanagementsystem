package com.ems.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import org.springframework.stereotype.*;

import com.ems.model.Employee;

import org.hibernate.query.*;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.*;


@Repository
public class EmployeeDAOImpl implements EmployeeDAO {
	
	@Autowired
	private EntityManager entityManager;

	@Override
	public List<Employee> get() {
		// TODO Auto-generated method stub
		Session currentSession=entityManager.unwrap(Session.class);
		String s="from Employee";
		
		Query<Employee> query =currentSession.createQuery(s,Employee.class);
		
	 List<Employee> employees= query.getResultList();
		
		return employees;
	}

	@Override
	public Employee get(int id) {
		
		Session currentSession=entityManager.unwrap(Session.class);
		  Employee employee= currentSession.get(Employee.class, id);
		 
		  return employee;
	}

	@Override
	public void registerEmployee(Employee employee) {
		Session currentSession=entityManager.unwrap(Session.class);
		   currentSession.save(employee);
	}
	@Override
	public void deleteEmployee(int id) {
		
		Session currentSession=entityManager.unwrap(Session.class);
		Employee employee= currentSession.get(Employee.class, id);
		  System.out.println("employee"+employee.getId());
		  currentSession.delete(employee);
		
	}
	@Override
	public String login(Employee employee) {
	Session currentSession=entityManager.unwrap(Session.class);
//		  User userdata= currentSession.get(User.class, user.getUsername());
//		  System.out.println("user"+userdata);
		  //return user;
	System.out.println("password"+ employee.getPassword());
	System.out.println("user"+employee);
	Employee u1 = null;
	try {
		u1=(Employee)currentSession.createQuery("from Employee where username = :username").setParameter("username",employee.getUsername()).getSingleResult();
	}
	catch(NoResultException nre) {
		
	}
		 if(u1 == null) {
			 return "0"; // user not found!
		 }
		 if(u1.getPassword().equals(employee.getPassword()))
		 {
			 return "1"; // login success;
		 }
		 else
		 {
			 return "2"; // password incorrect
		 }
		
		
	}

	}

