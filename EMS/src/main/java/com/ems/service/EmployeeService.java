package com.ems.service;

import java.util.List;

import com.ems.model.Employee;


public interface EmployeeService {

	 List<Employee> get();
	 Employee get(int id);
	 void registerEmployee(Employee user);
	 void deleteEmployee(int id);
	 String login(Employee user);
}
