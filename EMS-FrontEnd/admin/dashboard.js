if(localStorage.getItem("admin") == null){
    window.location.href="../home/index.html";
}
document.getElementById("welcome").innerHTML="Hello, "+ localStorage.getItem('admin');

function logout(){
 localStorage.removeItem('admin');
 window.location.href="../home/index.html";

}

var allEmployees;
getAllEmployees();
function getAllEmployees(){
    var xmlHttpRequest= new XMLHttpRequest();
    xmlHttpRequest.open('get', "http://localhost:14096/api/allEmployees");
    xmlHttpRequest.setRequestHeader('Content-type', 'application/json');
    xmlHttpRequest.send();
       xmlHttpRequest.onreadystatechange= function(){
           if(xmlHttpRequest.readyState == 4 && xmlHttpRequest.status == 200){
                console.log('responseText', this.responseText);
                allEmployees= JSON.parse(this.responseText);
                console.log('response',allEmployees);
               var username=document.getElementById('username');
                    var options="";
                allEmployees.forEach(function(user){
                               if(user.project == null){
                              options+="<option>"+user.username+"</option>";
                                }
                });
                username.innerHTML=options;
                showResultsAllEmployees(allEmployees);
                
              }
    }
    }
    function showResultsAllEmployees(allEmployees){
        console.log('allEmployees', allEmployees);

        var tbody=document.getElementById("tbody");

        var tablebodydata="";
         for(var i=0;i<allEmployees.length;i++){
              var employee=allEmployees[i];
              console.log('project', employee.project);
                if(employee.project != null){
           tablebodydata+= "<tr><td>"+employee.username+"</td>" + "<td>"+employee.role+"</td>"+ "<td>"+employee.project+"</td>"+ "<td>"+employee.client+"</td></tr>";
                }
         }
     tbody.innerHTML=tablebodydata;

    }

    function saveResult(){
        event.preventDefault();
       var username= getValueById("username");
       var project =getValueById("project");
       var client =getValueById("client");
       var role =getValueById("role");

       var employeeObject=getEmployeeByUserName(username);
         employeeObject.project=project;
         employeeObject.client=client;
         employeeObject.role=role;

         var xmlHttpRequest= new XMLHttpRequest();
xmlHttpRequest.open('put', "http://localhost:14096/admin/updateEmployee");
xmlHttpRequest.setRequestHeader('Content-type', 'application/json');
xmlHttpRequest.send(JSON.stringify(employeeObject));
   xmlHttpRequest.onreadystatechange= function(){
       if(xmlHttpRequest.readyState == 4 && xmlHttpRequest.status == 200){
            console.log('responseText', this.responseText);
            var response= JSON.parse(this.responseText);
            console.log('response', response);
            if(response.project != null){
                getAllEmployees();
            }
    }
}
    }
     function getValueById(id){
          return document.getElementById(id).value;
     }
     function getEmployeeByUserName(username){
            for(var i=0;i<allEmployees.length;i++){
                if(allEmployees[i].username == username){
                       return allEmployees[i];  
                }
            }

     }