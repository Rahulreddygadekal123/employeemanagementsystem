package com.ems.controller;

import org.springframework.web.bind.annotation.*;

import com.ems.model.Employee;

import com.ems.service.EmployeeService;

import org.springframework.beans.factory.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/api")
public class EmployeeController {

	
	@Autowired
	private EmployeeService employeeService;
	
	@CrossOrigin
	@GetMapping("/allEmployees")
	public List<Employee> get(){
		
		return employeeService.get();
		
	}
	
	@CrossOrigin
	@PostMapping("/register")
	public Employee register(@RequestBody Employee employee) {
		employeeService.registerEmployee(employee);
		return employee;
		
	}
	
	
	@GetMapping("/getEmployeeById/{id}")
	public Employee getUser(@PathVariable int id) {
		  return employeeService.get(id);
		
	}
	
	@DeleteMapping("/deleteemployeeById/{id}")
	public void deleteUser(@PathVariable int id) {
		System.out.println("Inside delete user");
		 employeeService.deleteEmployee(id);
		
	}
	
	@CrossOrigin
	@PostMapping("/login")
	public String loginEmployee(@RequestBody Employee employee) {
		return employeeService.login(employee);
	}
	
}
