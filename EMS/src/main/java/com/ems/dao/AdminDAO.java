package com.ems.dao;

import java.util.List;

import com.ems.model.Employee;



public interface AdminDAO {
	 List<Employee> allEmployees();
	 void updateEmployee(Employee employee);
}
