package com.ems.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.*;
import org.springframework.transaction.annotation.Transactional;

import com.ems.dao.EmployeeDAO;
import com.ems.model.Employee;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	
	@Autowired
	private EmployeeDAO employeeDAO;
	
	@Transactional
	@Override
	public List<Employee> get() {
		return employeeDAO.get();
	}

	@Transactional
	@Override
	public Employee get(int id) {
		   
		return employeeDAO.get(id);
	}
	@Transactional
	@Override
	public void registerEmployee(Employee employee) {
		// TODO Auto-generated method stub
		 employeeDAO.registerEmployee(employee);
		
	}
	
	@Transactional
	@Override
	public void deleteEmployee(int id) {
		System.out.println("ID"+id);
		// TODO Auto-generated method stub
		employeeDAO.deleteEmployee(id);
		
	}
	@Transactional
	@Override
	public String login(Employee employee) {
		// TODO Auto-generated method stub
		 return employeeDAO.login(employee);
		
	}

}
