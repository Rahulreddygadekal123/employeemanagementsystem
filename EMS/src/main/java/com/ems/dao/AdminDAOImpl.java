package com.ems.dao;

import java.util.List;
import javax.persistence.EntityManager;
import org.springframework.stereotype.*;

import com.ems.model.Employee;

import org.hibernate.query.*;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.*;

@Repository
public class AdminDAOImpl implements AdminDAO {
	
	@Autowired
	private EntityManager entityManager;

	@Override
	public List<Employee> allEmployees() {
		Session currentSession=entityManager.unwrap(Session.class);
		String s="from Employee";
		Query<Employee> query =currentSession.createQuery(s,Employee.class);
	 List<Employee> employees= query.getResultList();
		return employees;
	}
	@Override
	public void updateEmployee(Employee employee) {
		Session currentSession=entityManager.unwrap(Session.class);
		   currentSession.update(employee);
	}

}
