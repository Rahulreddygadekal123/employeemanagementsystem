package com.ems.controller;


import org.springframework.web.bind.annotation.*;

import com.ems.model.Employee;
import com.ems.service.AdminService;

import org.springframework.beans.factory.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/admin")
public class AdminController {

	@Autowired
	 private AdminService adminService;
	 
	// http://localhost:12096/admin/allStudents
	@CrossOrigin
	@GetMapping("/allEmployees")
	public List<Employee> getAllEmployees(){	
		return adminService.allEmployees();
	}
	@CrossOrigin
	@PutMapping("/updateEmployee")
	 public Employee updateEmployee(@RequestBody Employee employee) {
		 adminService.updateEmployee(employee);
		   return employee;
	 }
	
}
